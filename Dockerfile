FROM ubuntu:xenial
MAINTAINER Ovidiu-Florin BOGDAN (ovidiu.b13@gmail.com)

RUN apt update && \
    apt build-dep -y qtbase5-dev && \
    rm -rf /var/lib/apt/lists/*
COPY packages_to_install.txt /packages_to_install.txt
RUN apt update && \
    apt install -y `cat /packages_to_install.txt | tr "\n" " "` && \
    rm -rf /var/lib/apt/lists/* && \
    rm /packages_to_install.txt

ENV KDESRC=/source KDEBUILD=/build KDEINSTALL=/install

RUN useradd -m developer && \
    echo 'developer ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    mkdir -p $KDESRC $KDEBUILD $KDEINSTALL && \
    chown developer:developer -R $KDESRC $KDEBUILD $KDEINSTALL

COPY gitconfig.txt /home/developer/.gitconfig

USER developer

# Set-up kdesrc-build
RUN sudo mkdir /kdesrc-build && \
    sudo chown developer:developer -R /kdesrc-build && \
    git clone kde:kdesrc-build /kdesrc-build
COPY kdesrc-buildrc /home/developer/.kdesrc-buildrc

VOLUME ["/source", "/build", "/install"]

ENV QTDIR=/usr \
    CMAKE_PREFIX_PATH=$KDEINSTALL:$CMAKE_PREFIX_PATH \
    XDG_DATA_DIRS=$KDEINSTALL/share:$XDG_DATA_DIRS:/usr/share \
    XDG_CONFIG_DIRS=$KDEINSTALL/etc/xdg:$XDG_CONFIG_DIRS:/etc/xdg \
    PATH=$KDEINSTALL/bin:$QTDIR/bin:$PATH \
    QT_PLUGIN_PATH=$KDEINSTALL/lib/plugins:$KDEINSTALL/lib64/plugins:$KDEINSTALL/lib/x86_64-linux-gnu/plugins:$QTDIR/plugins:$QT_PLUGIN_PATH \
    QML2_IMPORT_PATH=$KDEINSTALL/lib/qml:$KDEINSTALL/lib64/qml:$KDEINSTALL/lib/x86_64-linux-gnu/qml:$QTDIR/qml \
    QML_IMPORT_PATH=$QML2_IMPORT_PATH \
    KDE_SESSION_VERSION=5 \
    KDE_FULL_SESSION=true

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
