KDE-build
===

A docker image, all set up for building KDE software.

This image is meant to have the set-up already made, in order to build and install into my system any KDE application from it's git sources, and not have to interfere with my base system.

Currently it includes a script to build KDevelop.

This is inspired from Kevin Funk's blog post: http://kfunk.org/2016/02/16/building-kdevelop-5-from-source-on-ubuntu-15-10/

