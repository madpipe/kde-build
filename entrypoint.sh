#!/bin/bash

KSB_update=$KDESRC/.kdesrc-build_update
: "${KSB_update_interval:=30}" # Default update interval to 30 days
: "${DISABLE_KSB_UPDATE_PROMPT:=false}"

function _curent_epoch() {
    # get the number of days since 1970-01-01 00:00:00 UTC
    echo $(( `date +%s` / 60 / 60 / 24 ))
}

function _update_last_update() {
    echo "KSB_LAST_UPDATE=`_curent_epoch`" > $KSB_update
}

function _update_ksb() {
    echo "Running git pull on kdesrc-build..."
    git -C /kdesrc-build pull
    _update_last_update
}

function check_ksb_updates() {
    if [[ -f $KSB_update ]]; then
        source $KSB_update
        : "${KSB_LAST_UPDATE:=0}"

        if [ $KSB_LAST_UPDATE -eq 0 ]; then
            _update_last_update && return 0
        fi

        epoch_diff=$(( $(_curent_epoch) - $KSB_LAST_UPDATE ))
        if [ $epoch_diff -gt $KSB_update_interval ]; then
            if [ "$DISABLE_KSB_UPDATE_PROMPT" = "true" ]; then
                _update_ksb
            else
                echo -e "[kdesrc-build] Would you like to check for updates? [Y/n]: \c"
                read line
                if [[ "$line" == Y* ]] || [[ "$line" == y* ]] || [ -z "$line" ]; then
                    _update_ksb
                else
                    _update_last_update
                fi
            fi
        fi
    else
        # create the $KSB_update file
        _update_last_update
    fi
}

check_ksb_updates

/kdesrc-build/kdesrc-build $@
